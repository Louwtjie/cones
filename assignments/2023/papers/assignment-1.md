# Assignment

- Course Title: **Software Tools for Data Science**
- Course Code: **STD912S**
- Assessment: First Assignment
- Released on: 01/09/2023
- Due Date: 15/09/2023

# Problem

## Problem Description

In this assignment, your task is to install a three-node cluster of [Apache Flink](https://flink.apache.org). You will document all the steps taken during the installation. 

## Assessment Criteria

We will follow the criteria below to assess the problem:

- Steps followed during installation. (15%)
- Evidence of healthy running cluster. (70%)
- Configuration during installation. (15%)

# Submission Instructions

- This assignment is to be completed by a group of at most three students.
- For each group, all documentation should be submitted to a repository created on [Gitlab](https://about.gitlab.com).
- The submission date is Friday, September 15 2023, at midnight. Please note that _commits_ after that deadline will not be accepted. Therefore, a submission will be assessed based on the repository's clone at the deadline.
- Groups that fail to submit on time will face a penalty (5 marks per day).
- A group which fails to submit will be awarded the mark 0.
- Each group is expected to present the project at an agreed date after the submission deadline.
- In the case of plagiarism (groups copying from each other or submissions copied from the Internet), all submissions involved will be awarded the mark 0, and each student will receive a warning.
- Each group is expected to communicate their details by Tuesday, September 5 2023, so that their credentials and the address of the nodes are communicated by that date.
