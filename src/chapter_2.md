# Chapter 2

This chapter presents cluster computing. Cluster computing is a topic of **distributed computing**. It focuses on how to assemble several machines and perform some advanced computation. In the context of big data or data science, cluster computing studies how to use a network of computer to speed up **data processing**. In this chapter, we discuss the imotivations behind this paradigm shift, explore a few examples and focus on specific tools.

## Motivations

There are various motivations behind moving a data infrastructure to a cluster environment. Here, we discuss two of these motivatitions. First, the relational model, despite its successes, is tailored for transactional operations. However, when facing data operations not organised within a transaction, with large volumes of data and at a higher velocity, the relational model proves inadequate. Furthermore, data is usually stored on disk and loaded in memory for processing. Consequently, large volumes of data increases the input-output (I/O) operations and hinders the overall cost of data processing. It might be more beneficial to keep the data in memory longer than it is usually done.

To address these limitations, a solution might be to scale up; extend the capabilities (memory, processing, storage) of the server handling the data operations. But, this approach only represents a temporaty fix to the problem. An alternative is to scale out: to turn to extendible collection of commodity hardware, connect them via a network and deploy a cluster to support the data pipeline.

## Overview of Cluster Compuping

The transition of data processing from a centralised to a cluster environment requires some adjustments. Indeed, distributing the data and thus the computation on several nodes in the cluster requires another programming model. The most common approach is the **Map/Reduce** model. As the name suggests, Map/Reduce is organised in two phases. First, the **Map** phase, where a __parallelisable__ part of the computation is executed on all the nodes where the data has been distributed. Once the Map  phase completes, all the intermediate results are then aggregated into the final result: the **Reduce** phase. You can read more about Map/Reduce in [[1]](#1). The Map/Reduce model can be implemented with a tool like [_Hadoop_](https://hadoop.apache.org).

The Map/Reduce model is acutally suitable for a **batch** execution: gather all the data in one or several files and delay the execution until a scheduled time. Note that during the execution, no invervention is required between the various commands. However, although a batch processing proves useful in many use cases, it is inadequate in other cases. For example, when the decision makers in an organisation wish to see the sales trends in real-time. In such a case, the processing can no longer be delayed. Here, we can turn to [_Spark_](https://spark.apache.org). A reader interesed in Spark's inner working is referred to [[2]](#2). Finally, in some cases, the data pipeline is a touch more complex. For example, combining both a batch and real-time layers in the data pipeline. This complex problem has been addressed by introducing an independent architecture: the **Lambda architecture**. It splits the processing architecture in three layers: batch serving and speed.

- the batch layer executes functions at scheduled time and updates its dataset at each iteration, including all new data came ine;
- the speed layer processes fresh data as they are ingested in the system;
- the serving layer merges the results from the two previous layers and provides a real-time and batch views.

The image below illustrates the architecture. For each layer, one can choose an implementation tool to fulfil the expected function.
![image](lambda-example.png)

In the remainder of this chapter, we discuss two tools that provide a cluster environment for data computation.

## Tools
### Apache Spark

Apache Spark is a multi-language tool for processing data. Its main abstraction is the resilient distributed dataset (RDD). An RDD is a distributed immutable collection of objects. The RDDs can be stored in memory and computations applied to them. The second abstraction in Apache Spark is the directed acyclic graph (DAG), which helps transform computation into an acyclic graph before execution.

Apache Spark's architecture uses a master-slave architecture with the following components:

- a driver, which runs a master node and accesses the spark context which contains the core functionality;
- a manager, that manages the cluster and oversees the executions of various jobs;
- worker nodes which hosts the jobs using executors that actually execute the tasks.

To install Apache Spark, one can follow the the instructions at <https://spark.apache.org/downloads.html>. In this document, we will take a different approach to installing Apache Spark. We will use [Ansible](https://www.ansible.com) to automate the installation and cofiguration of an Apache Spark standalone cluster.

First, you need to install Ansible on the machines in your cluster. The installation steps for Cent OS 7 are listed below

```bash
yum update

yum install epel-release

yum install ansible
```
Next, create a user on each machine that will run the Ansible playbook. Note that the same ansible user should be created on all machines. 

```bash
sudo adduser cbot

sudo passwd cbot
```
Finally, configure the ssh access between users

```bash
ssh-keygen -t rsa -b 8192

ssh-copy-id username@remote-host
```

## References

<a id="1">[1]</a>
Li, Junhao and Zhang, Hang (2019).
Blaze: Simplified High Performance Cluster Computing.
arXiv Pre Printing

<a id="2">[2]</a>
Zaharia, Matei and Chowdhury, Mosharaf and Franklin, Michael J. and Shenker, Scott and Stoica, Ion (2010)
Spark: Cluster Computing with Working Sets.
HotCloud'10: Proceedings of the 2nd USENIX conference on Hot topics in cloud computing.

